Facilitar Home Office
=====================

El aislamiento social obliga a trasladar el trabajo a modalidades remotas.

Pocas personas cuentan con la cultura laboral y herramientas para llevarlo a cabo de forma eficiente.

De no lograrlo, la presión económica podría echar abajo la modalidad de trabajo remoto y con ello el aislamiento social.


Acciones
--------

- Tutoriales orientados a empresarios y empleados que les faciliten adoptar el trabajo remoto

- Talleres virtuales para aprender a hacer home office

- Recomendar apps de trabajo remoto que funcionan bien en conjunto

- Recomendar de hardware/servicio de internet eficientes para el home office

- Artículos acerca de casos de éxito
