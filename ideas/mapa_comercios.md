# Un Mapa de Comercios en Xalapa

Siguiendo la idea de los issues [#1](https://gitlab.com/arturomf94/covid19xalapa/-/issues/1) [#2](https://gitlab.com/arturomf94/covid19xalapa/-/issues/2) podemos hacer una página web que, utilizando un mapa con https://leafletjs.com/ y un directorio con los datos de los comercios que se registren, permita a la gente que la utilice saber dónde hay abastecimiento de ciertos productos.

1. Comerciante:

- Deberá marcar su ubicación
- Deberá listar los productos y precio (una lista sencilla pe. *Papel higiénico 4 rollos*- precio: $25, *alcohol 70° 250ml* precio:$20). La finalidad de colocar el precio es que sea transparente.

1. Publico:

- Se podrá buscar por producto, y mostrar las tiendas mas cercanas que tengan existencia y su precio, con iconografia sencilla.
- Calificar la veracidad de la información por parte de la gente que acuda para evaluar la honestidad del comerciante y evitar engaños.

La finalidad no es la comercialización del producto sino ayudarle a encontrar a las personas lo que necesitan cerca de ellos, algo parecido a la gasolina en el waze.